package handson.fifteen.handson15;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import handson.fifteen.handson15.models.Customer;
import handson.fifteen.handson15.repositories.CustomerRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class TestTest {
    @Autowired
    private CustomerRepository customerRepository;

    @Test
    public void testAddUser() {
        Customer customer = new Customer();

        customer.setAvatar("/sdfsdfsdf");

        customer.setEmail("email@gmail.com");

        customer.setFirstName("fist name");

        customer.setLastName("last name");

        customer.setPhone("+62 8099-935-088");

        Customer savedCustomer = customerRepository.save(customer);

        Assertions.assertTrue(savedCustomer.getId() > 0);
        Assertions.assertNotNull(savedCustomer);
    }
}

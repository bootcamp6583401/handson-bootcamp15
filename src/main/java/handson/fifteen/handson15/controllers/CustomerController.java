package handson.fifteen.handson15.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import handson.fifteen.handson15.models.Address;
import handson.fifteen.handson15.models.Customer;
import handson.fifteen.handson15.services.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService employeeService;

    @Autowired
    public CustomerController(CustomerService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public Iterable<Customer> getAll() {
        return employeeService.getAll();
    }

    @PostMapping
    public Customer save(@RequestBody Customer employee) {
        return employeeService.save(employee);
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable int id) {
        return employeeService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        employeeService.deleteById(id);
    }

    @PutMapping("/{id}")
    public Customer updateById(@PathVariable int id, @RequestBody Customer employee) {
        return employeeService.updateById(id, employee);
    }

    @PutMapping("/{id}/address")
    public Customer updateById(@PathVariable int id, @RequestBody Address address) {
        return employeeService.setAddress(id, address);
    }
}

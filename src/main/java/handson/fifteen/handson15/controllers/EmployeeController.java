package handson.fifteen.handson15.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import handson.fifteen.handson15.models.Address;
import handson.fifteen.handson15.models.Employee;
import handson.fifteen.handson15.services.EmployeeService;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public Iterable<Employee> getAll() {
        return employeeService.getAll();
    }

    @PostMapping
    public Employee save(@RequestBody Employee employee) {
        return employeeService.save(employee);
    }

    @GetMapping("/{id}")
    public Employee getById(@PathVariable int id) {
        return employeeService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        employeeService.deleteById(id);
    }

    @PutMapping("/{id}")
    public Employee updateById(@PathVariable int id, @RequestBody Employee employee) {
        return employeeService.updateById(id, employee);
    }

    @PutMapping("/{id}/address")
    public Employee updateById(@PathVariable int id, @RequestBody Address address) {
        return employeeService.setAddress(id, address);
    }
}

package handson.fifteen.handson15.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import handson.fifteen.handson15.models.Service;
import handson.fifteen.handson15.services.ServiceService;

@RestController
@RequestMapping("/services")
public class ServiceController {

    private ServiceService addressService;

    @Autowired
    public ServiceController(ServiceService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    public Iterable<Service> getAll() {
        return addressService.getAll();
    }

    @PostMapping
    public Service save(@RequestBody Service address) {
        return addressService.save(address);
    }

    @GetMapping("/{id}")
    public Service getById(@PathVariable int id) {
        return addressService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        addressService.deleteById(id);
    }

    @PutMapping("/{id}")
    public Service updateById(@PathVariable int id, @RequestBody Service address) {
        return addressService.updateById(id, address);
    }
}

package handson.fifteen.handson15.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import handson.fifteen.handson15.models.OrderRequest;
import handson.fifteen.handson15.models.ShopOrder;
import handson.fifteen.handson15.services.OrderItemService;
import handson.fifteen.handson15.services.OrderService;
import handson.fifteen.handson15.services.ServiceService;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService, ServiceService serviceService,
            OrderItemService orderItemService) {
        this.orderService = orderService;
    }

    @GetMapping
    public Iterable<ShopOrder> getAll() {
        return orderService.getAll();
    }

    // @PostMapping
    // public ShopOrder save(@RequestBody ShopOrder order) {
    // return orderService.save(order);
    // }

    @PostMapping
    public ShopOrder save(@RequestBody OrderRequest orderRequest) {

        return orderService.complexSave(orderRequest);
    }

    @GetMapping("/{id}")
    public ShopOrder getById(@PathVariable int id) {
        return orderService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        orderService.deleteById(id);
    }

    @PutMapping("/{id}")
    public ShopOrder updateById(@PathVariable int id, @RequestBody ShopOrder order) {
        return orderService.updateById(id, order);
    }

}

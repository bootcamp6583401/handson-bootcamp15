package handson.fifteen.handson15.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.fifteen.handson15.models.ShopOrder;

public interface OrderRepository extends CrudRepository<ShopOrder, Integer> {

}
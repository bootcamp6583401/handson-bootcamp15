package handson.fifteen.handson15.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.fifteen.handson15.models.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {

}
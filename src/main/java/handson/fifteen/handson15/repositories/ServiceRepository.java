package handson.fifteen.handson15.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.fifteen.handson15.models.Service;

public interface ServiceRepository extends CrudRepository<Service, Integer> {

}
package handson.fifteen.handson15.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.fifteen.handson15.models.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}
package handson.fifteen.handson15.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import handson.fifteen.handson15.models.OrderItem;

public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {
    List<OrderItem> findByOrderId(int order_id);

}
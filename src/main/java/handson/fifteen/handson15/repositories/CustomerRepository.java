package handson.fifteen.handson15.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.fifteen.handson15.models.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

}
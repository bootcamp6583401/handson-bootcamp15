package handson.fifteen.handson15.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import handson.fifteen.handson15.repositories.ServiceRepository;

@Service
public class ServiceService {
    private final ServiceRepository employeeRepository;

    @Autowired
    public ServiceService(ServiceRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Iterable<handson.fifteen.handson15.models.Service> getAll() {
        return employeeRepository.findAll();
    }

    public handson.fifteen.handson15.models.Service save(handson.fifteen.handson15.models.Service employee) {
        return employeeRepository.save(employee);
    }

    public handson.fifteen.handson15.models.Service getById(int id) {
        return employeeRepository.findById(id).get();
    }

    public void deleteById(int id) {
        employeeRepository.deleteById(id);
    }

    public handson.fifteen.handson15.models.Service updateById(int id,
            handson.fifteen.handson15.models.Service employee) {
        Optional<handson.fifteen.handson15.models.Service> employeeRes = employeeRepository.findById(id);

        if (employeeRes.isPresent()) {
            employee.setId(id);
            return employeeRepository.save(employee);
        }

        return null;
    }

}

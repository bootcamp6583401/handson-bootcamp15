package handson.fifteen.handson15.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import handson.fifteen.handson15.models.Address;
import handson.fifteen.handson15.repositories.AddressRepository;

@Service
public class AddressService {
    private final AddressRepository addressRepository;

    @Autowired
    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public Iterable<Address> getAll() {
        return addressRepository.findAll();
    }

    public Address save(Address address) {
        return addressRepository.save(address);
    }

    public Address getById(int id) {
        return addressRepository.findById(id).get();
    }

    public void deleteById(int id) {
        addressRepository.deleteById(id);
    }

    public Address updateById(int id, Address address) {
        Optional<Address> addressRes = addressRepository.findById(id);

        if (addressRes.isPresent()) {
            address.setId(id);
            addressRepository.save(address);
        }

        return null;
    }
}

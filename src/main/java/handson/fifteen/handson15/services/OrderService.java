package handson.fifteen.handson15.services;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import handson.fifteen.handson15.models.OrderItem;
import handson.fifteen.handson15.models.OrderRequest;
import handson.fifteen.handson15.models.ShopOrder;
import handson.fifteen.handson15.repositories.OrderRepository;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    private final ServiceService serviceService;

    private final OrderItemService orderItemService;

    @Autowired
    public OrderService(OrderRepository orderRepository, OrderItemService orderItemService,
            ServiceService serviceService) {
        this.orderRepository = orderRepository;
        this.orderItemService = orderItemService;
        this.serviceService = serviceService;
    }

    public Iterable<ShopOrder> getAll() {
        return orderRepository.findAll();
    }

    public ShopOrder save(ShopOrder order) {
        return orderRepository.save(order);
    }

    public ShopOrder getById(int id) {
        return orderRepository.findById(id).get();
    }

    public void deleteById(int id) {
        orderRepository.deleteById(id);
    }

    public ShopOrder updateById(int id, ShopOrder order) {
        Optional<ShopOrder> orderRes = orderRepository.findById(id);

        if (orderRes.isPresent()) {
            order.setId(id);
            return orderRepository.save(order);
        }

        return null;
    }

    public ShopOrder complexSave(OrderRequest orderRequest) {
        ShopOrder order = save(orderRequest.getShopOrder());

        Set<OrderItem> orderItems = orderRequest.getOrderItems();

        handson.fifteen.handson15.models.Service service = serviceService.getById(orderRequest.getServiceId());

        for (OrderItem item : orderItems) {
            item.setService(service);
            item.setShopOrder(order);
        }
        Iterable<OrderItem> savedOrderItems = orderItemService.saveAll(orderItems);
        order.setOrderItems(StreamSupport.stream(savedOrderItems.spliterator(), false)
                .collect(Collectors.toSet()));

        return order;
    }

}

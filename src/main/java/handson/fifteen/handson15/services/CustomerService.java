package handson.fifteen.handson15.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import handson.fifteen.handson15.models.Address;
import handson.fifteen.handson15.models.Customer;
import handson.fifteen.handson15.repositories.AddressRepository;
import handson.fifteen.handson15.repositories.CustomerRepository;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    private final AddressRepository addressRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository, AddressRepository addressRepository) {
        this.customerRepository = customerRepository;
        this.addressRepository = addressRepository;
    }

    public Iterable<Customer> getAll() {
        return customerRepository.findAll();
    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer getById(int id) {
        return customerRepository.findById(id).get();
    }

    public void deleteById(int id) {
        customerRepository.deleteById(id);
    }

    public Customer updateById(int id, Customer customer) {
        Optional<Customer> customerRes = customerRepository.findById(id);

        if (customerRes.isPresent()) {
            customer.setId(id);
            return customerRepository.save(customer);
        }

        return null;
    }

    public Customer setAddress(int id, Address address) {
        Optional<Customer> customerRes = customerRepository.findById(id);
        if (customerRes.isPresent()) {
            Customer customer = customerRes.get();

            Address newAddress = addressRepository.save(address);

            customer.setAddress(newAddress);
            return customerRepository.save(customer);
        }

        return null;

    }
}

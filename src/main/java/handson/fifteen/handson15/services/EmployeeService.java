package handson.fifteen.handson15.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import handson.fifteen.handson15.models.Address;
import handson.fifteen.handson15.models.Employee;
import handson.fifteen.handson15.repositories.AddressRepository;
import handson.fifteen.handson15.repositories.EmployeeRepository;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    private final AddressRepository addressRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, AddressRepository addressRepository) {
        this.employeeRepository = employeeRepository;
        this.addressRepository = addressRepository;
    }

    public Iterable<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Employee getById(int id) {
        return employeeRepository.findById(id).get();
    }

    public void deleteById(int id) {
        employeeRepository.deleteById(id);
    }

    public Employee updateById(int id, Employee employee) {
        Optional<Employee> employeeRes = employeeRepository.findById(id);

        if (employeeRes.isPresent()) {
            employee.setId(id);
            return employeeRepository.save(employee);
        }

        return null;
    }

    public Employee setAddress(int id, Address address) {
        Optional<Employee> employeeRes = employeeRepository.findById(id);
        if (employeeRes.isPresent()) {
            Employee employee = employeeRes.get();

            Address newAddress = addressRepository.save(address);

            employee.setAddress(newAddress);
            return employeeRepository.save(employee);
        }

        return null;

    }
}

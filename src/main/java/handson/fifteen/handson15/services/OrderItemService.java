package handson.fifteen.handson15.services;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import handson.fifteen.handson15.models.OrderItem;
import handson.fifteen.handson15.repositories.OrderItemRepository;

@Service
public class OrderItemService {
    private final OrderItemRepository orderItemRepository;

    @Autowired
    public OrderItemService(OrderItemRepository orderItemRepository) {
        this.orderItemRepository = orderItemRepository;
    }

    public Iterable<OrderItem> getAll() {
        return orderItemRepository.findAll();
    }

    public OrderItem save(OrderItem order) {
        return orderItemRepository.save(order);
    }

    public OrderItem getById(int id) {
        return orderItemRepository.findById(id).get();
    }

    public void deleteById(int id) {
        orderItemRepository.deleteById(id);
    }

    public OrderItem updateById(int id, OrderItem order) {
        Optional<OrderItem> orderRes = orderItemRepository.findById(id);

        if (orderRes.isPresent()) {
            order.setId(id);
            return orderItemRepository.save(order);
        }

        return null;
    }

    public Iterable<OrderItem> saveAll(Set<OrderItem> orderItems) {
        return orderItemRepository.saveAll(orderItems);
    }

}

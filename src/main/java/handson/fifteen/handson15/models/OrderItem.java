package handson.fifteen.handson15.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String itemName;

    private double weight;

    private int amount;

    @ManyToOne()
    @JoinColumn(name = "shop_order_id")
    @JsonBackReference(value = "shopOrder-items")
    private ShopOrder order;

    @ManyToOne()
    @JoinColumn(name = "service_id")
    @JsonBackReference(value = "service-items")
    private Service service;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setShopOrder(ShopOrder order) {
        this.order = order;
    }

    public void setService(Service service) {
        this.service = service;
    }

}
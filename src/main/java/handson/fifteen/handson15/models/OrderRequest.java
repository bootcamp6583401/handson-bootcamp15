package handson.fifteen.handson15.models;

import java.util.Set;

public class OrderRequest {
    private Set<OrderItem> orderItems;
    private ShopOrder shopOrder;
    private int service_id;

    public OrderRequest(Set<OrderItem> orderItems, ShopOrder shopOrder, int service_id) {
        this.orderItems = orderItems;
        this.shopOrder = shopOrder;
        this.service_id = service_id;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public ShopOrder getShopOrder() {
        return shopOrder;
    }

    public int getServiceId() {
        return service_id;
    }

}
